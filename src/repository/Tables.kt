package io.threethirtythree.repository

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object EntriesTable : Table() {
    val entryId: Column<Long> = long("id").autoIncrement().primaryKey()
    val entryCreated = varchar("created", 50)
    val entryTitle = varchar("title", 125)
    val entryText = text("content")
}

object UserInfoTable : Table() {
    val userId: Column<Long> = long("userId").autoIncrement().primaryKey()
    val firstName = varchar("firstName", 50)
    val lastName = varchar("lastname", 50)
    val nickName = varchar("nickname", 50).nullable()
    val age = integer("age")
    val interests = varchar("interests", 100)
}
