package io.threethirtythree.repository

import io.threethirtythree.model.EntryModel
import io.threethirtythree.model.UserInfo

interface Repository {
    suspend fun addEntry(entryModel: EntryModel): Boolean
    suspend fun getEntry(entryId: Long): EntryModel?
    suspend fun deleteEntry(entryId: Long): Boolean
    suspend fun getAllEntries(): String
    suspend fun getAllEntriesCount(): Int

    suspend fun addUser(userInfo: UserInfo): Boolean
}