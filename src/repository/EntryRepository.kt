package io.threethirtythree.repository

import com.google.gson.Gson
import io.threethirtythree.db.DBFactory.dbQuery
import io.threethirtythree.model.EntryModel
import io.threethirtythree.model.UserInfo
import io.threethirtythree.repository.EntriesTable.entryCreated
import io.threethirtythree.repository.EntriesTable.entryId
import io.threethirtythree.repository.EntriesTable.entryText
import io.threethirtythree.repository.EntriesTable.entryTitle
import io.threethirtythree.repository.UserInfoTable.age
import io.threethirtythree.repository.UserInfoTable.firstName
import io.threethirtythree.repository.UserInfoTable.interests
import io.threethirtythree.repository.UserInfoTable.lastName
import io.threethirtythree.repository.UserInfoTable.nickName
import io.threethirtythree.repository.UserInfoTable.userId
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class EntryRepository : Repository {

    override suspend fun addUser(userInfo: UserInfo) = dbQuery {
        UserInfoTable.insert { user ->
            user[firstName] = userInfo.firstName
            user[lastName] = userInfo.lastName
            user[nickName] = userInfo.nickName
            user[age] = userInfo.age
            user[interests] = userInfo.interests
        }.resultedValues
    }.isNullOrEmpty()

    /**
     * Gets a count of all the rows in the Entries table.
     */
    override suspend fun getAllEntriesCount() = dbQuery {
        EntriesTable.selectAll().count()
    }

    override suspend fun getAllEntries(): String {
        val entryList = mutableListOf<EntryModel>()
        dbQuery {
            entryList.addAll(EntriesTable.selectAll().map { EntryModel.ModelMapper.from(it) })
        }
        return Gson().toJson(entryList)
    }

    /**
     * Adds a row in the Entries table.
     */
    override suspend fun addEntry(entryModel: EntryModel) =
        dbQuery {
            EntriesTable.insert { entry ->
                entry[entryCreated] = entryModel.entryCreated
                entry[entryTitle] = entryModel.entryTitle
                entry[entryText] = entryModel.entryText
            }.resultedValues
        }.isNullOrEmpty()

    /**
     * Deletes a row in the Entries table if it exists.
     */
    override suspend fun deleteEntry(entryId: Long) = dbQuery {
        EntriesTable.deleteWhere {
            EntriesTable.entryId eq (entryId)
        }
    } > 0

    /**
     * Displays a full row from the Entries table.
     */
    override suspend fun getEntry(entryId: Long) = dbQuery {
        EntriesTable.select(EntriesTable.entryId eq entryId)
            .map { resultRowToEntryModel(it) }
            .singleOrNull()
    }

    /**
     * Helper methods to convert a ResultRow into an EntryModel
     */
    private fun resultRowToEntryModel(row: ResultRow?): EntryModel? {
        if (row == null) {
            return null
        }
        return EntryModel(
            entryId = row[entryId],
            entryCreated = row[entryCreated],
            entryTitle = row[entryTitle],
            entryText = row[entryText]
        )
    }

    private fun resultRowToUserInfo(row: ResultRow?): UserInfo? {
        if (row == null) {
            return null
        }
        return UserInfo(
            userId = row[userId],
            firstName = row[firstName],
            lastName = row[lastName],
            nickName = row[nickName],
            age = row[age],
            interests = row[interests]
        )
    }
}