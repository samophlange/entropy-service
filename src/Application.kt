package io.threethirtythree

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.gson.gson
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.sessions.Sessions
import io.ktor.util.KtorExperimentalAPI
import io.threethirtythree.db.DBFactory
import io.threethirtythree.model.EntryModel
import io.threethirtythree.model.UserInfo
import io.threethirtythree.repository.EntryRepository

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)


@KtorExperimentalAPI
fun Application.module() {

    DBFactory.init()
    val db = EntryRepository()

    install(DefaultHeaders)
    install(Sessions)
    install(CallLogging)
    install(DataConversion)
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
            serializeNulls()
        }
    }

    install(CORS) {
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Delete)
        anyHost()
    }
    install(Compression) {
        gzip()
    }

    /// Actions pertaining to defined routes
    routing {

        get("/") {
            call.respondText("This is just an API homie. Not much to see here :)")
        }
        /// Get a listing of all the journal entries (filtering available)
        get("/entry/count") {
            // TODO: 2020-08-09 only return for authed user
            val numEntries = db.getAllEntriesCount()
            call.respondText("Hey there have been $numEntries added to the table!")
        }

        get("/entry/get/{id}") {
            val entryModel = db.getEntry(call.parameters["id"]?.toLong()!!)
            entryModel?.let { it1 -> call.respond(it1) }
        }

        get("/entry/list") {
            // TODO: 2020-08-09 only return for authed user
            val entries = db.getAllEntries()
            call.respond(entries)
        }

        post("/entry/create") {
            // TODO: 2020-08-09 only return for authed user
            val entryDto = call.receive<EntryModel>()
            db.addEntry(entryDto)
            call.respond(HttpStatusCode.Created)
        }

        delete("/entry/delete/{id}") {
            // TODO: 2020-08-09 only return for authed user
            val removed = db.deleteEntry(call.parameters["id"]?.toLong()!!)
            if (removed) call.respond(HttpStatusCode.OK)
            else call.respond(HttpStatusCode.NotFound)
        }

        post("/user/register") {
            val userRequestInfo = call.receive<UserInfo>()
            db.addUser(userRequestInfo)
        }
    }
}
