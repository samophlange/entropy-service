package io.threethirtythree.model

import io.threethirtythree.repository.EntriesTable.entryCreated
import io.threethirtythree.repository.EntriesTable.entryId
import io.threethirtythree.repository.EntriesTable.entryText
import io.threethirtythree.repository.EntriesTable.entryTitle
import org.jetbrains.exposed.sql.ResultRow

/**
 * This will be a temporary spot to hold all of our initial ideas about how we want to represent out data,
 * before we get to the DB stage!
 * */


/**
 * Data class that represents the information pertiniant to a User of the app.
 */
data class UserInfo(
    val userId: Long = 0L,
    val firstName: String,
    val lastName: String,
    val nickName: String?,
    val age: Int,
    val interests: String
)

/**
 * Data class for a particular users journal entry.
 */
data class EntryModel(val entryId: Long = 0L, val entryCreated: String, val entryTitle: String, val entryText: String) {
    object ModelMapper {
        fun from(row: ResultRow) =
            EntryModel(row[entryId], row[entryCreated], row[entryTitle], row[entryText])
    }
}
